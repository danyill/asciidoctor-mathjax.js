/* eslint-env mocha */

const mathJaxProcessor = require('../src/asciidoctor-mathjax')
const asciidoctor = require('@asciidoctor/core')()

const chai = require('chai')
const { expect } = require('chai')
const { initSnapshotManager } = require('mocha-chai-jest-snapshot')
chai.use(initSnapshotManager)

const PRINT = false

describe('stem mathjax tests', () => {
  it('asciidoc default stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`

A matrix can be written as 

[stem]
++++
[[a,b],[c,d]]((n),(k))
sqrt(4) = 2
sqrt(9) = 3
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2].
Another square root looks like stem:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:
:a: a
:b: b
:c: c
:d: d
:n: n
:k: k
:four: 4
:two: 2
:squareroot: sqrt
:expr: sqrt(9)=3

A matrix can be written as stem:a[[[{a},{b}\\],[{c},{d}\\]\\](({n}),({k}))].
A square root looks like stem:a[{squareroot}({four}) = {two}].
Another square root looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose list test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

* A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2]
* Another square root looks like stem:[sqrt(9) = 3]
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*"]
|===

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]
|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table with head, foot test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*",options="header,footer"]
|===
|Header matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Header square root: stem:[sqrt(4) = 2]
|Header square root: stem:[sqrt(9) = 3]

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]

|Footer matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Footer square root: stem:[sqrt(4) = 2]
|Footer square root: stem:[sqrt(9) = 3]

|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default section test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
= The Title
:stem:

== A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].

== A square root looks like stem:[sqrt(4) = 2].

=== Another square root looks like stem:[sqrt(9) = 3].

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

Some vector components are

[stem]
++++
u=\\frac{-y}{x^2+y^2}\\,,\\quad
v=\\frac{x}{x^2+y^2}\\,,\\quad\\text{and}\\quad
w=0\\,.
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath
:x: x
:y: y
:iint: \\iint
:expr: v=\\frac{{x}}{x^2+y^2}

An integral can be written as stem:a[{iint} {x}{y}^2\\,dx\\,dy ].
A fraction looks like stem:a[u=\\frac{-{y}}{{x}^2+{y}^2}].
Another fraction looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose list test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

* An integral can be written as stem:[\\iint xy^2\\,dx\\,dy].
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
* Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath table test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

.Math Table
[cols="3*"]
|===

|An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath table with head, foot test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

.Math Table
[cols="3*",options="header,footer"]
|===
|Header integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|Header fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Header another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|Footer integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|Footer fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Footer another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath section test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
= The Title
:stem: latexmath

== An integral can be written as stem:[\\iint xy^2 dx dy ].

== A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}]

=== Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('asciimath blocks', () => {
  it('explicit asciimath block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

A matrix can be written as 

[asciimath]
++++
[[a,b],[c,d]]((n),(k))
sqrt(4) = 2
sqrt(9) = 3
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciimath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

A matrix can be written as asciimath:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like asciimath:[sqrt(4) = 2].
Another square root looks like asciimath:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
describe('latexmath blocks', () => {
  it('latexmath stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

Some vector components are

[latexmath]
++++
u=\\frac{-y}{x^2+y^2}\\,,\\quad
v=\\frac{x}{x^2+y^2}\\,,\\quad\\text{and}\\quad
w=0\\,.
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('explicit latexmath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

An integral can be written as latexmath:[\\iint xy^2\\,dx\\,dy ].
A fraction looks like latexmath:[u=\\frac{-y}{x^2+y^2}].
Another fraction looks like latexmath:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('fraction with multiline source', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`[latexmath]
++++
\\frac{(byte[{\\tt 8d}] \\times {\\tt 10000} + byte[{\\tt 8e}] \\times
  {\\tt 100} + byte[{\\tt 8f}]) - {\\tt 100000}}{{\\tt 100000}}
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('reset attribute value tests', () => {
  it('asciidoc default prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:
:expr: sqrt(4)=2

// A matrix can be written as stem:a[[[{a},{b}\\],[{c},{d}\\]\\](({n}),({k}))].
A square root looks like stem:a[{expr}].

:expr: sqrt(9)=3

Another square root looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
describe('stem attribute not required', () => {
  it('asciidoc default prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`

A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2].
Another square root looks like stem:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('latexmath inline tests', () => {
  it('fraction with multiple escaped close brackets', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert('latexmath:[\\frac{byte[{\\tt 2e}\\] \\times 256 + byte[{\\tt 2f}\\]}{100.0}]', { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
